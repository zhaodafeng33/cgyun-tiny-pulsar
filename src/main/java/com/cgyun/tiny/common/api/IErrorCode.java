package com.cgyun.tiny.common.api;

/**
 * 封装API的错误码
 * @author panpan668706@163.com
 * @version 1.0
 * @date 2021/8/14 3:50 下午
 */
public interface IErrorCode {
    long getCode();

    String getMessage();
}
