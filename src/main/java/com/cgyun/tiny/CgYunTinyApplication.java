package com.cgyun.tiny;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CgYunTinyApplication {

    public static void main(String[] args) {
        SpringApplication.run(CgYunTinyApplication.class, args);
    }

}
