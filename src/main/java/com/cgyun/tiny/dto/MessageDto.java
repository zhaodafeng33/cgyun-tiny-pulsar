package com.cgyun.tiny.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author panpan668706@163.com
 * @version 1.0
 * @date 2021/8/14 3:50 下午
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MessageDto {
    private Long id;
    private String content;
}
